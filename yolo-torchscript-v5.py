import tvm
from tvm import relay
import numpy as np
import torch
import torchvision

model_name = "yolov5s.pt"

model = torch.hub.load('ultralytics/yolov5', 'yolov5s')

input_shape = [1,3,1280, 720]
input_data = torch.randn(input_shape)
scripted_model = torch.jit.trace(model,input_data).eval()


