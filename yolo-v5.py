import os
import numpy as np
import matplotlib.pyplot as plt
import sys
import pdb
import tvm
from PIL import Image
from tvm import te, autotvm
from tvm.contrib import graph_executor
from tvm import relay
from tvm.runtime import container
from tvm.runtime import vm as vm_rt
from tvm.relay import testing
from tvm.relay import vm

import tvm.relay.testing.yolo_detection
import tvm.relay.testing.darknet

from tvm.contrib.download import download_testdata
from util import load_test_image, download_model_zoo,parse_options, get_device_arch, get_device_attributes, get_device_type, get_tvm_target, parse_cmd_options
import sys

argv=sys.argv[1:]

device, logfile = parse_cmd_options(argv)

model_dir ="./"
model_name = "yolov5s-fp16.tflite"

# model_dir = download_model_zoo(model_dir, model_name)

tflite_model_file = os.path.join(model_dir, model_name)
tflite_model_buf = open(tflite_model_file, "rb").read()

# Get TFLite model from buffer
try:
    import tflite
    tflite_model = tflite.Model.GetRootAsModel(tflite_model_buf, 0)
except AttributeError:
    import tflite.Model
    tflite_model = tflite.Model.Model.GetRootAsModel(tflite_model_buf, 0)

dtype="float32"
image_data = load_test_image(dtype, 640, 640)

input_tensor = "serving_default_input_1:0"
input_shape = (1, 640, 640, 3)
input_dtype = dtype

# Parse TFLite model and convert it to a Relay module
mod, params = relay.frontend.from_tflite(tflite_model,
                                         shape_dict={input_tensor: input_shape},
                                         dtype_dict={input_tensor: input_dtype})
#desired_layouts = {'nn.conv2d': ['NCHW', 'default'], 'nn.max_pool2d': ['NCHW', 'default']}
#seq = tvm.transform.Sequential([relay.transform.RemoveUnusedFunctions(),relay.transform.ConvertLayout(desired_layouts)])

#with tvm.transform.PassContext(opt_level=3):
#    mod = seq(mod)

tvm_target = get_tvm_target(device, get_device_type(), get_device_arch(), get_device_attributes())

cpu_target = "llvm"
tvm_targets = tvm.target.Target(tvm_target, host=cpu_target)

cpudevice = tvm.runtime.cpu()

if logfile is not None:
    with autotvm.apply_history_best(logfile):
        with tvm.transform.PassContext(opt_level=3):
            graph_mod = relay.build(mod, tvm_targets, params=params)
else:
    with tvm.transform.PassContext(opt_level=3):
        graph_mod = relay.build(mod, tvm_targets, params=params)

lib = graph_mod.get_lib()
params = graph_mod.get_params()

# Create a runtime executor module
module = graph_executor.GraphModule(graph_mod["default"](cpudevice))

# Feed input data
module.set_input(input_tensor, tvm.nd.array(image_data))

# Feed related params
module.set_input(**params)

thresh = 0.5
mns_thresh= 0.45

#ftimer = module.module.time_evaluator("run", cpudevice, number=1, repeat=1)
module.run()
#prof_res = np.array(ftimer().results) * 1000  # multiply 1000 for converting to millisecond
#print("%-20s %-7s %-19s (%s)" % (model_name, device, "%.2f ms" % np.mean(prof_res), "%.2f ms" % np.std(prof_res)))
tvm_out = []

pdb.set_trace()
numberout=module.get_num_outputs()
thing = nil
for i in range(numberout):
    thing = module.get_output(i)




##
for i in range(3):
    layer_out = {}
    layer_out["type"] = "Yolo"
    # Get the yolo layer attributes (n, out_c, out_h, out_w, classes, total)
    layer_attr = module.get_output(i * 4 + 3).numpy()
    layer_out["biases"] = module.get_output(i * 4 + 2).numpy()
    layer_out["mask"] = module.get_output(i * 4 + 1).numpy()
    out_shape = (layer_attr[0], layer_attr[1] // layer_attr[0], layer_attr[2], layer_attr[3])
    layer_out["output"] = module.get_output(i * 4).numpy().reshape(out_shape)
    layer_out["classes"] = layer_attr[4]
    tvm_out.append(layer_out)

# do the detection and bring up the bounding boxes
img = tvm.relay.testing.darknet.load_image_color(img_path)
_, im_h, im_w = img.shape
dets = tvm.relay.testing.yolo_detection.fill_network_boxes(
    (netw, neth), (im_w, im_h), thresh, 1, tvm_out
)
last_layer = net.layers[net.n - 1]
tvm.relay.testing.yolo_detection.do_nms_sort(dets, last_layer.classes, nms_thresh)

coco_name = "coco.names"
coco_url = REPO_URL + "data/" + coco_name + "?raw=true"
font_name = "arial.ttf"
font_url = REPO_URL + "data/" + font_name + "?raw=true"
coco_path = download_testdata(coco_url, coco_name, module="data")
font_path = download_testdata(font_url, font_name, module="data")

with open(coco_path) as f:
    content = f.readlines()

names = [x.strip() for x in content]

tvm.relay.testing.yolo_detection.show_detections(img, dets, thresh, names, last_layer.classes)
tvm.relay.testing.yolo_detection.draw_detections(
    font_path, img, dets, thresh, names, last_layer.classes
)
plt.imshow(img.transpose(1, 2, 0))
plt.show()



#ftimer = module.module.time_evaluator("run", cpudevice, number=1, repeat=10)
#prof_res = np.array(ftimer().results) * 1000  # multiply 1000 for converting to millisecond
#print("%-20s %-7s %-19s (%s)" % (model_name, device, "%.2f ms" % np.mean(prof_res), "%.2f ms" % np.std(prof_res)))
print(tvm_target)
